<?php

namespace App\Http\Traits;
trait DataTrait
{
    public function dataArrayOrders()
    {
        return [
           [
               'Order_ID' => 1001,
               'Order_Date' => '2007-05-01 12:10:10',
               'Sales_Type' => 'Normal',
               'Products' => [
                   [
                       'Order_Product_ID' => 2000,
                       'Order_ID' => 1001,
                       'Item_Name' => 'Radio',
                       'Normal_Price' => 800.00,
                       'Promotion_Price' => 712.99,
                   ]
               ]
           ],
            [
                'Order_ID' => 1002,
                'Order_Date' => '2007-05-07 05:28:55',
                'Sales_Type' => 'Normal',
                'Products' => [
                    [
                        'Order_Product_ID' => 2001,
                        'Order_ID' => 1002,
                        'Item_Name' => 'Portable Audio',
                        'Normal_Price' => 16.00,
                        'Promotion_Price' => 15.00,
                    ],
                    [
                        'Order_Product_ID' => 2002,
                        'Order_ID' => 1002,
                        'Item_Name' => 'THE SIMS',
                        'Normal_Price' => 9.99,
                        'Promotion_Price' => 8.79,
                    ]
                ]
            ],
            [
                'Order_ID' => 1003,
                'Order_Date' => '2007-05-19 17:17:00',
                'Sales_Type' => 'Promotion',
                'Products' => [
                    [
                        'Order_Product_ID' => 2003,
                        'Order_ID' => 1003,
                        'Item_Name' => 'Radio',
                        'Normal_Price' => 800.00,
                        'Promotion_Price' => 712.99,
                    ]

                ]
            ],
            [
                'Order_ID' => 1004,
                'Order_Date' => '2007-05-22 22:47:16',
                'Sales_Type' => 'Promotion',
                'Products' => [
                    [
                        'Order_Product_ID' => 2004,
                        'Order_ID' => 1004,
                        'Item_Name' => 'Scanner',
                        'Normal_Price' => 124.00,
                        'Promotion_Price' => 120.00,
                    ]

                ]
            ],
            [
                'Order_ID' => 1005,
                'Order_Date' => '2007-05-27 08:15:07',
                'Sales_Type' => 'Promotion',
                'Products' => [
                    [
                        'Order_Product_ID' => 2005,
                        'Order_ID' => 1005,
                        'Item_Name' => 'Portable Audio',
                        'Normal_Price' => 16.00,
                        'Promotion_Price' => 15.00,
                    ],
                    [
                        'Order_Product_ID' => 2006,
                        'Order_ID' => 1005,
                        'Item_Name' => 'Radio',
                        'Normal_Price' => 800.00,
                        'Promotion_Price' => 712.99,
                    ]

                ]
            ],
            [
                'Order_ID' => 1006,
                'Order_Date' => '2007-06-01 06:35:59',
                'Sales_Type' => 'Normal',
                'Products' => [
                    [
                        'Order_Product_ID' => 2007,
                        'Order_ID' => 1006,
                        'Item_Name' => 'Camcorders',
                        'Normal_Price' => 359.00,
                        'Promotion_Price' => 303.00,
                    ],
                    [
                        'Order_Product_ID' => 2008,
                        'Order_ID' => 1006,
                        'Item_Name' => 'Radio',
                        'Normal_Price' => 800.00,
                        'Promotion_Price' => 712.99,
                    ]

                ]
            ],
        ];
    }

    public function dataArrayCustomersAndSalesOrder(){
        return [
            [
                'name'=>'Suraj',
                'email'=>'suraj@gmail.com',
            ],
            [
                'name'=>'Kiran',
                'email'=>'kiran@gmail.com',
            ]
        ];
    }
}
