<?php

namespace App\Http\Controllers;

use App\Models\Reward\Customer;
use App\Models\Reward\CustomerOrderReward;
use App\Models\Reward\SalesOrderReward;
use App\Models\Reward\SalesOrders;


class MainController extends Controller
{


    /**
     * Get three most popular food ingredients
     * Each data frame consists of no more than 1000 rows.
     * The instructions are complete and data is valid.
     * Do not make any assumptions beyond the problem statement
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function defineThreeMostPopularFoodIngredients()
    {
        try {
            $foodIngredients = $this->getDataFromCSVFile(1000, 3);
            return view('food_ingredients', compact('foodIngredients'));
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Read and get content or data from CSV File
     * @param $maxRows
     * @param $limitResults
     * @return mixed
     * @throws \Exception
     */
    private function getDataFromCSVFile($maxRows, $limitResults)
    {
        $csvFile = public_path('./files/recipe.csv');//file path

        try {
            $f = fopen($csvFile, 'r');
            if (!$f) throw new \Error('Error to open the file ');
            $row = 1;
            $rows = [];
            while (($line = fgetcsv($f, $maxRows, ",")) !== false) {   //get only 1000 line rows
                $rows[] = $line;
            }

            return $this->mappedCSVDataAsFoodIngredients($rows, $limitResults);
        } catch (\Exception $e) {
            throw new \Exception($e);
        } finally {

            if (@$f) fclose($f);
        }
    }


    /**
     * mapped csv data as required for food ingredients
     * @param $rows
     * @param $limitResults
     * @return mixed
     */
    private function mappedCSVDataAsFoodIngredients($rows, $limitResults)
    {

        $foodIngredients = [];
        foreach ($rows as $row) {
            if (@$row[3]) {
                foreach (explode(',', $row[3]) as $string) {
                    array_push($foodIngredients, trim($string));
                }
            }
        }

        asort($foodIngredients);//sorting to asc
        $foodIngredientsArrayCountValue = array_count_values($foodIngredients);//re arranged array with count values
        arsort($foodIngredientsArrayCountValue);// again sorting value in  desc order
        return array_slice($foodIngredientsArrayCountValue, 0, $limitResults); //get results form given limit Results

    }

    /**
     *Determine the number of words in a given sentence where the word is a sequence of letters, ('a-z', 'A-Z').
     * Words may contain one or more of these ( . ) - period, ( , ) - comma, ( ? ) - question mark, ( ! ) - exclamation point.
     * Separation of words will be by one or more white spaces. Two words can be joined into one by hyphen while the other punctuation marks should be stripped.
     *Constraints 0 < length of s ≤ 105
     */
    public function countWords()
    {
        $string = 'Apple is so scarce nowadays, isn’t it?? I had to walk an hour to get a half-dozen.';
        $stringLimit = substr($string, 0, 105);//limit character to 105

        $filterArray = array_filter(explode(' ', $stringLimit), function ($value) {
            return $value && preg_match("/[’]/", $value) == false;

        });//split string with white space and filter words
        $filterArray = array_map(function ($value) {
            return preg_replace("/[.?!,]/", "", $value);//replace punctuation characters
        }, $filterArray);//

        return view('count_words', compact('filterArray'));


    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * select `orders`.`Order_ID`, SUM(CASE WHEN orders.Sales_Type = "Normal" THEN order_products.Normal_Price ELSE order_products.Promotion_Price END) as Total_Sales_Amount,COUNT(order_products.Order_ID) as Total_Order from `orders` inner join `order_products` on `orders`.`Order_ID` = `order_products`.`Order_ID` group by `Order_ID`
     */
    public function getTotalOrdersAndSalesAmount()
    {

        $orders = \DB::table('orders')
            ->join('order_products', 'orders.Order_ID', '=', 'order_products.Order_ID')
            ->groupBy('Order_ID')
            ->select('orders.Order_ID', \DB::raw('SUM(CASE WHEN orders.Sales_Type = "Normal" THEN order_products.Normal_Price ELSE order_products.Promotion_Price END) as Total_Sales_Amount,COUNT(order_products.Order_ID) as Total_Order'))
            ->get();
        return view('order', compact('orders'));
    }

    /**
     * Store Random Sales Order Amount
     * Store Reward Points to Customer
     */
    public function creditUserRewards()
    {
        try {
            //Customer Info
            $customerId = rand(1, 2); // we have two customers of id 1 and 2
            $customer = Customer::find($customerId);//get customer detail
            if (!$customer) throw  new \Error('Customer might not be stored,so we cannot credited reward amount to customer.');


            //Sales Order
            $salesOrderAmount = rand(1000, 5000);//generate random sales order amount
            $salesOrderStatus = 'Completed';
            $salesOrderUnit = 'NPR';
            $storeSalesOrderResp = SalesOrders::create(['customer_id' => 1, 'amount' => $salesOrderAmount, 'status' => $salesOrderStatus, 'unit' => $salesOrderUnit]);
            if (!$storeSalesOrderResp) throw  new \Error('Sales order could not be created,so we cannot credited reward amount to customer.');


            //Sales Reward Points
            if ($salesOrderStatus != 'Completed') throw  new \Error('Sales order was not completed,so we cannot credited reward amount to customer.');
            switch ($salesOrderUnit) {
                case 'NPR':
                    $salesOrderAmountInUSD = $salesOrderAmount / 116.80; // $1 is equal to 116.80;
                    //output 12.84
                    break;
                default:
                    $salesOrderAmountInUSD = $salesOrderAmount;
            }

            $rewardPoints = number_format($salesOrderAmountInUSD, 2);
            $createSalesOrderRewardResp = SalesOrderReward::create([
                'customer_id' => $customerId,
                'sales_order_id' => $storeSalesOrderResp->id,
                'reward_point' => $rewardPoints,
            ]);//store reward points to SalesOrderReward
            if (!$createSalesOrderRewardResp) throw new \Error('Could not be credited sales reward point.');


            //Customer Reward Points
            $expiredDate = date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 365 day"));
            $prevCustomerOrderReward = CustomerOrderReward::where('customer_id', $customerId)->first();
            $totalRewardPoints = $prevCustomerOrderReward ? $prevCustomerOrderReward->total_reward_point + $rewardPoints : $rewardPoints;
            $storeCustomerOrderReward=[
                'total_reward_point' =>$totalRewardPoints,
                'expired_date' => $expiredDate,
            ];
            if($prevCustomerOrderReward){
                $customerRewardResp = CustomerOrderReward::where('customer_id',$prevCustomerOrderReward->customer_id)->update($storeCustomerOrderReward);//update reward points to SalesOrderReward
            }else{
                $storeCustomerOrderReward['customer_id'] = $customerId;
                $customerRewardResp = CustomerOrderReward::create($storeCustomerOrderReward);//create reward points to SalesOrderReward
            }


            if ($customerRewardResp) {
                die('Successfully, Credited reward points (' . $rewardPoints . ') on sales order amount (' . $salesOrderUnit . ' ' . $salesOrderAmount . ') to customer (' . $customer->name . ') with expired date (' . $expiredDate . ').');
            } else {
                throw new \Error('Could not be credited reward amount to customer.');
            }
        } catch (\Exception $e) {
            die('Error!!, ' . $e->getMessage());
        }

    }


}
