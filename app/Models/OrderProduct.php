<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table='order_products';
    protected $fillable=['Order_ID','Order_Product_Id','Normal_Price','Promotion_Price','Item_Name'];
}
