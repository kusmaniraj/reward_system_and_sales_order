<?php

namespace App\Models\Reward;

use Illuminate\Database\Eloquent\Model;

class CustomerOrderReward extends Model
{
    protected $table='customer_rewards';
    protected $fillable=['customer_id','id','total_reward_point','expired_date'];
}
