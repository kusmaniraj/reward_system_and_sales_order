<?php

namespace App\Models\Reward;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    protected $table='order_payments';
    protected $fillable=['customer_id','id','amount'];
}
