<?php

namespace App\Models\Reward;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table='customers';
    protected $fillable=['id','name','email'];
    public $timestamps = false;
}
