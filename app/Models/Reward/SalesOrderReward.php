<?php

namespace App\Models\Reward;

use Illuminate\Database\Eloquent\Model;

class SalesOrderReward extends Model
{
    protected $table='sales_order_rewards';
    protected $fillable=['customer_id','id','sales_order_id','reward_point'];
}
