<?php

namespace App\Models\Reward;

use Illuminate\Database\Eloquent\Model;

class SalesOrders extends Model
{
    protected $table='sales_orders';
    protected $fillable=['customer_id','id','unit','amount','status'];
}
