<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='orders';
    protected $fillable=['Order_ID','Order_Date','Sales_Type'];

    public function orderProducts(){
        return $this->hasMany(OrderProduct::class,'Order_ID','Order_ID');
    }
}
