<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome</title>
</head>
<body>
<h2 style="text-align: center">Welcome to Reward System and Sales Order </h2>
<br><br><br>
<h4>Routing List</h4>
<table border="1">
    <tr>
        <th>Route Name</th>
        <th>Url</th>
    </tr>
    <tr>
        <td>Three Most Popular Food Ingredients:</td>
        <td><a href="{{url("/three-popular-food-ingredients")}}">{{url("/three-popular-food-ingredients")}}</a></td>
    </tr>
    <tr>
        <td>Count String Words</td>
        <td><a href="{{url("/count-words")}}">{{url("/count-words")}}</a></td>
    </tr>
    <tr>
        <td>Total Order and Sales Amount:</td>
        <td><a href="{{url("/total-order-and-sales-amount")}}">{{url("/total-order-and-sales-amount")}}</a></td>
    </tr>
    <tr>
        <td>Credit User Rewards:</td>
        <td><a href="{{url("/credit-user-rewards")}}">{{url("/credit-user-rewards")}}</a></td>
    </tr>
</table>

</body>
</html>
