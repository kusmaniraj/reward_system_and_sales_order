<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Food Ingredients</title>
</head>
<body>
<div>
    <h3>Three Most Popular Food Ingredients <a style="margin-left: 20px" href="{{url('/')}}">Back</a></h3>
    <br>
    <table border="1">
        <thead>
        <tr>
            <th>Name</th>
            <th>Count</th>
        </tr>
        </thead>
        <tbody>
        @isset($foodIngredients)
            @forelse($foodIngredients as $name=>$count)
                <tr>
                    <td>{{$name}}</td>
                    <td>{{$count}}</td>
                </tr>
            @empty
                <tr><td colspan="2">No results found ....</td></tr>
            @endforelse
        @endisset
        </tbody>
    </table>
</div>
</body>
</html>
