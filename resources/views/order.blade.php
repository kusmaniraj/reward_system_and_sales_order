<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Orders</title>
</head>
<body>
<div>
    <h3>Total Orders and Sales Amount of All Orders <a style="margin-left: 20px" href="{{url('/')}}">Back</a></h3>
    <br>
    <table border="1">
        <thead>
        <tr>
            <th>Order Id</th>
            <th>Number of   Order</th>
            <th>Total Sales Amount</th>
        </tr>
        </thead>
        <tbody>
        @isset($orders)
            @forelse($orders as $order)
                <tr>
                    <td>{{$order->Order_ID}}</td>
                    <td>{{$order->Total_Order}}</td>
                    <td>{{$order->Total_Sales_Amount}}</td>

                </tr>
            @empty
                <tr><td colspan="2">No results found ....</td></tr>
            @endforelse
        @endisset
        </tbody>
    </table>
</div>
</body>
</html>
