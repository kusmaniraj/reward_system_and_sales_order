<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->bigInteger('Order_Product_ID')->primary()->unsigned();
            $table->unsignedBigInteger('Order_ID');
            $table->string('Item_Name');
            $table->float('Normal_Price');
            $table->float('Promotion_Price');
            $table->timestamps();
            $table->foreign('Order_ID')->references('Order_ID')->on('orders')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
