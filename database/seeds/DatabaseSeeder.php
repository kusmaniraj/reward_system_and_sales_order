<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    use \App\Http\Traits\DataTrait;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (\Schema::hasTable('orders')) {

            foreach ($this->dataArrayOrders() as $data) {

                $ordersData = $data;
                unset($ordersData['Products']);
                $insertedOrder = \DB::table('orders')->insert($ordersData);
                if ($insertedOrder) {
                    \DB::table('order_products')->insert($data['Products']);
                }
            }


        }
        if (\Schema::hasTable('customers')) {
            foreach ($this->dataArrayCustomersAndSalesOrder() as $data) {
                \DB::table('customers')->insert($data);
            }


        }
    }


}
