<?php
Route::get('/', function () {

    return view('welcome');
});
Route::get('/three-popular-food-ingredients', 'MainController@defineThreeMostPopularFoodIngredients');
Route::get('/count-words', 'MainController@countWords');
Route::get('/total-order-and-sales-amount', 'MainController@getTotalOrdersAndSalesAmount');
Route::get('/credit-user-rewards', 'MainController@creditUserRewards');
